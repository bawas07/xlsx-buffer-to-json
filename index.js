const xlsx = require('xlsx')

exports.transform = (buf) => {
    const wb =  xlsx.read(buf, {type:'buffer'})
    const docs = xlsx.utils.sheet_to_json(wb.Sheets.Sheet1)
    console.log(docs)
    return docs
}