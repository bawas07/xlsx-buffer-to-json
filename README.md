# README

this is an example for how to recieve a [.xls, .xlsx, .odt] file and turn it to json without save the file.

this code using was build on nodejs v8

I also use npm package xlsx to do the job

## how to use

```
const y = require('xlsx-buffer-to-json')

y.transform(data)
```